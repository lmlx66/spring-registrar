package com.wfg.spring;

import com.wfg.spring.springRegistrar.mapper.SelectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringRegistrarApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringRegistrarApplication.class, args);
        SelectMapper bean = applicationContext.getBean(SelectMapper.class);
        bean.selectOne();
        bean.batchSelect();
        bean.selectOne();
        Object selectMapper = applicationContext.getBean("selectMapperFactoryBean");
    }
}
