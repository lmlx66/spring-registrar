package com.wfg.spring.springRegistrar.annotation;

import org.springframework.stereotype.Indexed;

import java.lang.annotation.*;

/**
 * @author: 王富贵
 * @description: 自定义mapper注解
 * @createTime: 2023/11/19 13:51
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Indexed
public @interface WfgMapper {
    String value() default "";
}