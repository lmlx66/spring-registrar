package com.wfg.spring.springRegistrar.FactoryBean;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author: 王富贵
 * @description:
 * @createTime: 2023/11/05 14:59
 */
public class MyRegistrar implements ImportBeanDefinitionRegistrar {
        /**
         * @param importingClassMetadata 使用@Import导入本类的那个类的元数据
         * @param registry 注册器，可用于注册BeanDefinition，生成bean
         */
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        // 1.创建scanner对象
        // 注意第二个参数表示是否使用默认的过滤规则，默认过滤规则是检查类是否有spring的注解，如@Service，@Component等，有才会被扫描
        ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry, false);

        // 包含过滤器，满足条件才注入
        // 方法1 使用spring官方注解过滤器
        scanner.addIncludeFilter(new AnnotationTypeFilter(Component.class));

        // 方法2 自定义过滤器
        scanner.addExcludeFilter((metadataReader, metadataReaderFactory) ->
                metadataReader.getAnnotationMetadata().hasAnnotation("org.springframework.stereotype.Component"));

        // scanner.addExcludeFilter(); // 排除过滤器，满足条件排除注入

        // 2.进行扫描,会将包下满足规律规则的类注册进入registry中
        scanner.scan("com.wfg.core.producer");

        // 获取所有定义的beanDefinition
        Set<BeanDefinition> candidateComponents = scanner.findCandidateComponents("com.wfg.core.producer");
    }
}
