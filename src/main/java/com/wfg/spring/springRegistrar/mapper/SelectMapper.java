package com.wfg.spring.springRegistrar.mapper;

import com.wfg.spring.springRegistrar.annotation.WfgMapper;

/**
 * @author: 王富贵
 * @description:
 * @createTime: 2023/11/05 19:14
 */
@WfgMapper
public interface SelectMapper {
    void selectOne();

    void batchSelect();
}
