package com.wfg.spring.springRegistrar.configuration;

import com.wfg.spring.springRegistrar.FactoryBean.WfgMapperRegistrar;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = WfgMapperRegistrar.class)
public class TestRegistrarAutoConfiguration {
    // @Bean
    // public SelectMapperFactoryBean selectMapper() {
    //     return new SelectMapperFactoryBean("com.wfg.spring.springRegistrar.mapper.SelectMapper");
    // }
}
